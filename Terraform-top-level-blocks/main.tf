

resource "aws_vpc" "wittylight" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support

  tags = {
    Name = upper("wittylight-${terraform.workspace}")
  }
}

#Private subnets
resource "aws_subnet" "private_sub1" {
  vpc_id                  = local.vpc_id
  cidr_block              = var.private_sub1
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = upper("private-subnet-${terraform.workspace}")
  }
}

resource "aws_subnet" "private_sub2" {
  vpc_id                  = local.vpc_id
  cidr_block              = var.private_sub2
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name = upper("private-subnet2-${terraform.workspace}")
  }
}

#Public subnets
resource "aws_subnet" "public_sub1" {
  vpc_id                  = local.vpc_id
  cidr_block              = var.public_sub1
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = upper("public-subnet-${terraform.workspace}")
  }
}


#Public subnets
resource "aws_subnet" "public_sub2" {
  vpc_id                  = local.vpc_id
  cidr_block              = var.public_sub2
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = upper("public-subnet2-${terraform.workspace}")
  }
}

resource "aws_instance" "wittylight_instance" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = local.instance_type
  subnet_id     = aws_subnet.public_sub1.id
  key_name      = "wittykeys"
  # check firewall (secrurity group[22])
  # keypair
  # check vpc
  # store[] ...parameter store
  tags = {
    Name = var.instance_name
  }
}