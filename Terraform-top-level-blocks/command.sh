# "" = string
# '' = single string
# =  = equal
# [] = list
# {} = map, curly brackets
# () = parenthesis
# |  = pipe, grep, or 
# && = ampersand, ampersand
# #  = comment
# !  = alter a condition [true]
# ~  = tilde
# >  = greater than

# READme.md
#     terraform-docs markdown table .>README.md

# terraform init
# terraform validate
# terraform fmt
# terraform plan
# terraform apply
# terraform destroy
# terraform console
# terraform apply -target allows you to deploy a single resource
# terraform state list

# git status
# git add -A or git add .
# git commit -m "commit name"
# git push
# git reset
# git stash

## terraform functions
# count
# slice = > why
# length = > check the amount of item/s found in a list
# element = > 


# string => ""
# list => []
# map => {}
# bool => true/false
# number => "2"
# object =>

# workspace commands:
# terraform workspace list
# teterraform workspace new dev
# terraform workspace new sbx
# terraform workspace new prod
# terraform workspace delete to delete a workspace
# terraform workspace select dev
# terraform plan -var-file dev.tfvars
# terraform plan -var-file sbx.tfvars
# terraform plan -var-file prod.tfvars
# terraform apply -var-file dev.tfvars
# terraform apply -var-file sbx.tfvars
# terraform apply -var-file prod.tfvars
# terraform destroy -var-file dev.tfvars
# terraform destroy -var-file sbx.tfvars
# terraform destroy -var-file prod.tfvars