output "vpc_id" {
  description = "VPC ID"
  value       = aws_vpc.wittylight.id
}

output "private_sub1" {
  description = "Information for private subnet1"
  value       = aws_subnet.private_sub1.id
}

output "private_sub2" {
  description = "Information for private subnet2"
  value       = aws_subnet.private_sub2.id
}

output "public_sub1" {
  description = "Information for public subnet 1"
  value       = aws_subnet.public_sub1.id
}

output "aws_instance" {
  description = "Output for the instance"
  value       = aws_instance.wittylight_instance.id

}

output "arn" {
  description = "ARN of the server"
  value       = aws_instance.wittylight_instance.arn
}

output "server_name" {
  description = "Name (id) of the server"
  value       = aws_instance.wittylight_instance.id
}

