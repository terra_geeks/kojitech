variable "vpc_cidr" {
  type        = string
  description = "The cidr for the VPC"
}

variable "enable_dns_hostnames" {
  type        = bool
  description = "The type of hostnames to assign to instances in the subnet "
  default     = true
}

variable "enable_dns_support" {
  type        = bool
  description = "The option to enable dns support"
  default     = true
}

variable "vpc_name" {
  type        = string
  description = "The name of the VPC"
  default     = "wittylight_vpc"
}

# for private subnets, use odd numbers for CIDRs
variable "private_sub1" {
  type        = string
  description = "CIDR for private_sub1"
}

variable "private_sub2" {
  type        = string
  description = "CIDR for private_sub2"
}

variable "az_private_sub1" {
  type        = string
  description = "availability zone for private_sub1"
  default     = "us-east-1a"
}


variable "az_private_sub2" {
  type        = string
  description = "availability zone for private_sub2"
  default     = "us-east-1b"
}

variable "public_sub1" {
  type        = string
  description = "Name for private subnet 1"
  default     = "private_sub1"
}

variable "public_sub2" {
  type        = string
  description = "Name for private subnet 2"
  default     = "private_sub2"
}


variable "az_public_sub1" {
  type        = string
  description = "availability zone for public_sub1"
  default     = "us-east-1b"
}

variable "ami_id" {
  type        = string
  description = "Provide the ami_id"
  default     = "ami-0cff7528ff583bf9a"

}

variable "instance_type" {
  type        = string
  description = "instance type for ec2"
  default     = "t2.micro"

}

variable "instance_name" {
  type        = string
  description = "name of instance"
  default     = "WittyLight"

}
