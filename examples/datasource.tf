data "aws_key_pair" "example" {
  key_name = "wittykeys"
}

#data source to pull down available az's
data "aws_availability_zones" "available" {
  state = "available"
}

#data source to pull down available az's
data "aws_ami" "amazon_linux" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name = "name"

    values = [
      "amzn-ami-hvm-*-x86_64-gp2",
    ]
  }
}
