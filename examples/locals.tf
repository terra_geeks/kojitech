
locals {
  vpc_id            = aws_vpc.witty_light.id
  availability_zone = data.aws_availability_zones.available
  ami               = data.aws_ami.amazon_linux.id
  instance_type     = "t2.micro"

}