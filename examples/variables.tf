variable "vpc_cidr" {
  type        = string
  description = "The cidr for the VPC"
  default     = "10.0.0.0/16"
}

variable "enable_dns_hostnames" {
  type        = bool
  description = "The type of hostnames to assign to instances in the subnet "
  default     = true
}

variable "enable_dns_support" {
  type        = bool
  description = "The option to enable dns support"
  default     = true
}

variable "vpc_name" {
  type        = string
  description = "The name of the VPC"
  default     = "wittylight_vpc"
}

# for private subnets, use odd numbers for CIDRs
variable "cidr_block_privsub1" {
  type        = string
  description = "CIDR for privsub1"
  default     = "10.0.1.0/24"
}

variable "cidr_block_privsub2" {
  type        = string
  description = "CIDR for privsub2"
  default     = "10.0.3.0/24"
}

variable "az_privsub1" {
  type        = string
  description = "availability zone for privsub1"
  default     = "us-east-1a"
}


variable "az_privsub2" {
  type        = string
  description = "availability zone for privsub2"
  default     = "us-east-1b"
}

variable "private_sub1" {
  type        = string
  description = "Name for private subnet 1"
  default     = "private_sub1"
}

variable "private_sub2" {
  type        = string
  description = "Name for private subnet 2"
  default     = "private_sub2"
}

# for public subnets, use even numbers for CIDRs
variable "cidr_block_pubsub1" {
  type        = string
  description = "CIDR for pubsub1"
  default     = "10.0.0.0/24"
}

variable "az_pubsub1" {
  type        = string
  description = "availability zone for pubsub1"
  default     = "us-east-1b"
}
variable "public_sub1" {
  type        = string
  description = "Name for public subnet 1"
  default     = "public_sub1"
}

variable "ami_id" {
  type        = string
  description = "Provide the ami_id"
  default     = "ami-0cff7528ff583bf9a"

}

variable "instance_type" {
  type        = string
  description = "instance type for ec2"
  default     = "t2.micro"

}

variable "instance_name" {
  type        = string
  description = "name of instance"
  default     = "WittyLight"

}