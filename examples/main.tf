

resource "aws_vpc" "witty_light" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support

  tags = {
    Name = var.vpc_name
  }
}

#Private subnets
resource "aws_subnet" "private_sub1" {
  vpc_id                  = local.vpc_id
  cidr_block              = var.cidr_block_privsub1
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "private_sub1"
  }
}

resource "aws_subnet" "private_sub2" {
  vpc_id                  = local.vpc_id
  cidr_block              = var.cidr_block_privsub2
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "private_sub2"
  }
}

#Public subnets
resource "aws_subnet" "public_sub1" {
  vpc_id                  = local.vpc_id
  cidr_block              = var.cidr_block_pubsub1
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "public_sub1"
  }
}

resource "aws_instance" "wittylight_instance" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = local.instance_type
  subnet_id     = aws_subnet.public_sub1.id
  key_name      = "wittykeys"
  # check firewall (secrurity group[22])
  # keypair
  # check vpc
  # store[] ...parameter store
  tags = {
    Name = var.instance_name
  }
}