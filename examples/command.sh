# "" = string
# '' = single string
# =  = equal
# [] = list
# {} = map, curly brackets
# () = parenthesis
# |  = pipe, grep, or 
# && = ampersand, ampersand
# #  = comment
# !  = alter a condition [true]
# ~  = tilde
# >  = greater than

# READme.md
#     terraform-docs markdown table .>README.md

# terraform init
# terraform validate
# terraform fmt
# terraform plan
# terraform apply
# terraform destroy
# terraform console
# terraform apply -target allows you to deploy a single resource
# terraform state list

# git status
# git add -A or git add .
# git commit -m "commit name"
# git push
# git reset
# git stash
